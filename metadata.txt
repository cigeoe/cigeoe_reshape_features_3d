# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=CIGeoE Reshape Features 3D
qgisMinimumVersion=3.0
description=reshape a feature in 3D
version=1.0
author=Centro de Informação Geoespacial do Exército 
email=igeoe@igeoe.pt

about=This plugin reshape line or polygon with 3 dimensions using Inverse Distance Weighting (IDW) interpolation

tracker=https://gitlab.com/cigeoe/cigeoe_reshape_features_3d/-/issues
repository=https://gitlab.com/cigeoe/cigeoe_reshape_features_3d  
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=3d,vector,layers

homepage=https://gitlab.com/cigeoe/cigeoe_reshape_features_3d  
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

