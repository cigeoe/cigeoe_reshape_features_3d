# CIGeoE Reshape Features 3D



# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_reshape_features_3d” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  ![ALT](./images/image01.png)

  3 - Select “CIGeoE Reshape Features 3D”

  ![ALT](./images/image02.png)

  4 - After confirm the operation the plugin will be available in toolbar

  ![ALT](./images/image03.png)

- Method 2:

  1 - Compress the folder “cigeoe_reshape_features_3d” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - On "Layers Panel", select a layer, line or polygon and click on the plugin icon.

![ALT](./images/image04.png)

2 - Use the mouse left button to draw lines. When finished, use the right mouse button to execute the plugin.

![ALT](./images/image05.png)
![ALT](./images/image06.png)

3 - Using the "Current Edits" icon, one can roll back or save the result.

![ALT](./images/image07.png)

4 - Click the plugin icon to deativate it


# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
