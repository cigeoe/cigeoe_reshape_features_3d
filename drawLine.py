# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox 

from qgis.gui import QgsMessageBar, QgsMapTool, QgsRubberBand
from qgis.core import *
from qgis.utils import iface 

from math import *
import sys
import re
import math
from shapely.geometry import Point, LineString, MultiLineString, Polygon

class drawLine(QgsMapTool):
    
    def __init__(self, canvas, iface):  
        
        self.canvas = canvas
        self.iface = iface               # add iface for work with active layer       
        QgsMapTool.__init__(self,self.canvas)        
        
        self.rb=QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.PolygonGeometry)       
        #self.rb.setColor(QColor(60,151,255, 255)) # azul claro
        self.rb.setColor(QColor(100,100,255, 255))
        self.status = 0
        self.mover = 0
        self.point = None
        self.linePoints = []

        #return None
        
    # Left click to place points. Right click to confirm.
     
    def keyPressEvent(self, e):
        if e.matches(QKeySequence.Undo):
            if self.rb.numberOfVertices() > 1:
                self.rb.removeLastPoint()


    def canvasPressEvent(self,e):
        if e.button() == Qt.LeftButton:
            if self.status == 0:                
                self.rb.reset(QgsWkbTypes.LineGeometry)
                self.status = 1

                self.mover = 0
                self.point = self.toMapCoordinates(e.pos())
                self.linePoints.append(self.point)
                
            self.rb.addPoint(self.toMapCoordinates(e.pos()))
                       
            if self.mover == 2:
                self.point = self.toMapCoordinates(e.pos())
                self.linePoints.append(self.point)                
                self.mover = 1
                      
        else:
            if self.rb.numberOfVertices() > 1:
                self.status = 0 
                if self.mover != 1:
                    self.point = self.toMapCoordinates(e.pos())
                    self.linePoints.append(self.point) 
                    
                if len(self.linePoints) > 1:                           
                    self.drawLine(self.linePoints)                      # modulo : drawLine                    
                    self.linePoints=[] 
                    self.reset()                
                    #return None
            else:                
                self.linePoints=[] 
                self.reset()       
        #return None        
                  
    
    def canvasMoveEvent(self,e):
        if self.rb.numberOfVertices() > 0 and self.status == 1:
            self.rb.removeLastPoint(0)
            self.rb.addPoint(self.toMapCoordinates(e.pos())) 
            self.mover = 2     
         
        #return None

    def reset(self):
        self.status = 0
        self.rb.reset( True )
        

    def deactivate(self):
        self.rb.reset( True )
        QgsMapTool.deactivate(self)


    #*********************************************

    def segfeature(self, lista):  
        pointseg = []
        seg = []                
        npoint = 0
        for point in lista:  
            npoint += 1    
            if npoint == 2: 
                seg.append(point)
                pointseg.append(seg)
                seg = []
                npoint = 1                
            seg.append(point) 
        return pointseg  
        
    #---------------------------------

    def sortpoints(self, lista, point):  
        pos = 0     
        soma = 0
        for ind in range(len(lista)):  
            soma += 1
            if lista[ind] == point:                 
                pos = soma
        return pos      

    #----------------------------------       
   
    def euclidean_distance(self, x1, y1, x2, y2):
        return math.sqrt((x2-x1)**2 + (y2-y1)**2)       
        
    #----------------------------------   

    def idw(self, x1 , y1, listpoints, typefeat):     
        num = listpoints.numPoints()
        if typefeat == 2:
            num = num - 1
        
        sumValue = 0
        sumWeight = 0
        z1 = 0
        for k in range(num):
            xe = listpoints.xAt(k)
            ye = listpoints.yAt(k)
            ze = listpoints.zAt(k)            
            #distance = math.sqrt((x1-xe)**2 + (y1-ye)**2)
            distance = self.euclidean_distance(x1, y1, xe, ye) 
            if distance == 0:
                weight = 0
            else:
                #weight = 1 / (distance**2)
                weight = 1 / (distance)
            sumValue = sumValue + (ze*weight)
            sumWeight = sumWeight + weight

        if sumWeight == 0:
            z1 = 0
        else:    
            z1 = sumValue / sumWeight
        return z1

     #----------------------------------                 

    def points3D(self, lista, regex):
        xyzlist=[]  
        for i in range(len(lista)):
            coord = lista[i] 
            coord = re.findall(regex, coord)
            numcoord = len(coord)                        
            coordx = float(coord[0])
            coordy = float(coord[1])
            if numcoord != 2:
                coordz = float(coord[2])
            else:
                coordz = float(0)           
           
            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
            xyzlist.append(newcoord)  
        return xyzlist

    #----------------------------------               
   
    def interpointZ(self, x, y, geomlist, linestring_polygon):         
        for k in range(len(geomlist)):
            xe = linestring_polygon.xAt(k)
            ye = linestring_polygon.yAt(k)
            ze = linestring_polygon.zAt(k)            
            if x == xe and y == ye:
                coordx = float(xe)                    
                coordy = float(ye)   
                coordz = float(ze)                      
                newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)                
                break
        return newcoord
           
    #----------------------------------             

    def retiracarater(self, geom):         
        ini = geom.find("(")            # position of the first "("    
        ini = ini + 1
        fim = geom.rfind(")")           # position of the last ")"
        listgeom = geom[ini:fim]        # considers the characters between the two positions
        return listgeom  

    #----------------------------------     

    def retiracarater1(self, geom): 
        listgeom = []
        ini = geom.find("(")             
        ini = ini + 1
        fim = geom.rfind(")")          
        listgeom.append(geom[ini:fim])      # list
        return listgeom  

    
    ################################################################################            

    def drawLine(self, linePoints):
        regex = re.compile('[-+]?\d*\.\d+|\d+') 
        layer = self.iface.activeLayer()   
        
        pointsLine=[]         
        for i in range(len(linePoints)):
            coordx = float(linePoints[i][0])
            coordy = float(linePoints[i][1])                 
            newcoord=QgsPointXY(coordx,coordy)    
            pointsLine.append(newcoord)       

        # check if it's a simple geometry line and doesn't intersect with itself
        pointsLinexx=LineString(pointsLine)          
        if not pointsLinexx.is_simple:  
            QMessageBox.information(self.iface.mainWindow(), "Error", 'The drawn line is not a simple geometry.')            
            return  

        if layer.geometryType() != QgsWkbTypes.PolygonGeometry :
            if layer.geometryType() != QgsWkbTypes.LineGeometry :  
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The layer must be of line or polygon type .')  

        viewportPolyline = QgsGeometry().fromWkt(self.canvas.extent().asWktPolygon())        
        inViewFeats = layer.getFeatures(QgsFeatureRequest().setFilterRect(viewportPolyline.boundingBox()))
       

        featidentx = []
        featsActivex = []  
        featident = []
        featsActive = []                                # feature of layer active
        totgeom = 0
        fora = 90           # feature intercepted by the line, but do not contain points belonging to the line
        firstPoint = []


        if layer.geometryType() == QgsWkbTypes.LineGeometry :  
            
            #-------------------    INICIO   reshape line  --------------------------------------------------------     

            pointsLinex = QgsGeometry.fromPolylineXY(pointsLine)
           
            for feat in inViewFeats: 
                geom = feat.geometry()  
            
                if geom.intersects(pointsLinex): 
                    geom1 = geom.asPolyline() 
                
                    totgeom += 1       
                    
                    ident = feat.id()
                    featident.append(ident)
                    featsActive.append(feat)          
                    
            if totgeom == 0:
                QMessageBox.information(self.iface.mainWindow(), "Error", "The line draw does not intersects the feature.")
                return  
            
            totfeat = []          # features changed to update the layer
            firstfeat = 0         # indicates the feat with points 3D with the z coordinate to consider (first) to reserve  
        
            for i in range(len(featsActive)):  
                feat = featsActive[i]
                geom = feat.geometry()  

                firstfeat += 1
        
                #--------------   convert QgsLineString  to a list with the 3D coordinates
                geom1 = geom.asPolyline()            
                geom3D = geom.asWkt()                            # QgsLineString (LineStringZ)    coordenates (x,y,z)
 
                listgeom3D = self.retiracarater(geom3D)        
                listvertex = []
                vertex = listgeom3D
                vertex = vertex.split(",")
                listvertex.append(vertex)        
        
                geomlist=[] 
                geomlist.append(self.points3D(listvertex[0], regex))   
            
                linestring_line = QgsLineString()                # QgsLineString
                linestring_line.setPoints(geomlist[0])           # line vertices (x,y,z)

                #------------------------
        
                line0 = feat.geometry().asPolyline()           
                pointsLine1=[]                           
                for i in range(len(line0)):
                    pointsLine1.append(line0[i])               # list of polyline points (x,y)

                segLine = self.segfeature(pointsLine)          #  splits drawline into segments       
            
                segLine1 = self.segfeature(pointsLine1)        #  splits feature line into segments 
       
                #------------ calculate intersection points 
                interlistx = []              
                interPoints = []         
                posinterLine1 = []   
                distLine1 = [] 
                posinterLine = []              
                distLine = []       
                for ind in range(len(segLine1)):   
                    coordp0 = segLine1[ind][0][0] 
                    coordp1 = segLine1[ind][0][1]                
                    xxxLine1 = []   
                    xxxLine1.append(segLine1[ind][0])
                    xxxLine1.append(segLine1[ind][1])
           
                    segmentLine1 = QgsGeometry.fromPolylineXY(xxxLine1)      
            
                    for ind1 in range(len(segLine)): 
                        coordl0 = segLine[ind1][0][0] 
                        coordl1 = segLine[ind1][0][1]
                        xxxLine = []
                        xxxLine.append(segLine[ind1][0])
                        xxxLine.append(segLine[ind1][1])
                        segmentLine = QgsGeometry.fromPolylineXY(xxxLine)               
                            
                        if segmentLine.intersects(segmentLine1):  
                            inter = segmentLine.intersection(segmentLine1)
                            vertice = inter.asPoint()   
                            interPoints.append(vertice)                   # intersection point

                            posinterLine.append(ind1)                     # line segment containing the point                   
                            dist = self.euclidean_distance(coordl0, coordl1, vertice[0], vertice[1] )                    
                            distLine.append(dist)                                        
                      
                            posinterLine1.append(ind)                     # line1 segment containing the point      
                            dist = self.euclidean_distance(coordp0, coordp1, vertice[0], vertice[1] )                    
                            distLine1.append(dist)
                    
                            #----------  insert Z coordinate at intersection points (interPoints3D)
                            interlist = []                
                            interx = vertice.x()                    # atribute .x() (access the x coordinate)
                            intery = vertice.y()   
                            segment3D = segmentLine1.asPolyline() 
                            coordx = float(segment3D[0][0])            
                            coordy = float(segment3D[0][1]) 
                            newcoord = self.interpointZ(coordx, coordy, geomlist[0], linestring_line) 
                            interlist.append(newcoord)   
                   
                            coordx = float(segment3D[1][0])             
                            coordy = float(segment3D[1][1])   
                            newcoord = self.interpointZ(coordx, coordy, geomlist[0], linestring_line) 
                            interlist.append(newcoord)   
                    
                            interxx = QgsLineString()               # QgsLineString 
                            interxx.setPoints(interlist)                   
                   
                            typefeat = 1                            # 1 = line
                            interz = self.idw(interx, intery, interxx, typefeat)
                            #interz = self.idw(interx, intery, linestring_line, typefeat)   

                            coordx = float(interx)                    
                            coordy = float(intery)   
                            coordz = float(interz)                              
                            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                            interlistx.append(newcoord)                              

                if len(interPoints) < 2: 
                    QMessageBox.information(self.iface.mainWindow(), "Error", "To reshape a line it is necessary 2 points of intersection.")
                    return  

                interPoints3D = QgsLineString()                     # QgsLineString 
                interPoints3D.setPoints(interlistx)       
       
                #--------  insert intersection points in line (newLine)        
                interLinex = []  
                posinterLinex = []
         
                for ind in range(len(segLine)):
                    interdist = []  
                    interpoint = [] 
                    interpos = [] 
                    for ind1 in range(len(posinterLine)): 
                        if posinterLine[ind1] == ind:                     
                            interdist.append(distLine[ind1])                    
                            interpoint.append(interPoints[ind1])
                            interpos.append(posinterLine[ind1])

                    interdistx = interdist[:]
                    interdistx.sort()                           # sort descending order
        
                    #flgx= 0        
                    for ind in range(len(interdistx)):
                        for ind1 in range(len(interdist)):
                            if interdistx[ind] == interdist[ind1]:
                                interLinex.append(interpoint[ind1])
                                posinterLinex.append(interpos[ind1])            
     
                newLine = []        
                pospointnewLine = []  
                ordemPoint = []       
                flaginter = 0                           
                ordem = 0        
                for ind1 in range(len(segLine)):      
                 
                    if flaginter == 1:
                        newLine.append(segLine[ind1][0])
                        pospointnewLine.append(0)                  
                      
                    for ind in range(len(posinterLinex)):
                        if posinterLinex[ind] == ind1:
                            ordem += 1
                            newLine.append(interLinex[ind])                    
                            pospointnewLine.append(ordem)   
                            ordemPoint.append(interLinex[ind]) 
                            if ordem == len(interPoints): 
                                flaginter = 0
                            else:    
                                flaginter = 1
             
                #--------------   insert Z coordinate at other points (newLine)               
                newLinex = QgsGeometry.fromPolylineXY(newLine)           
                newLinexx = newLinex.asPolyline()

                pospointnewLine9 = pospointnewLine[:]
                linelist = []
                linePoints3D = []
                coordpoint = []
                for i in range(len(newLine)): 
                    if pospointnewLine[i] != 0:                        
                        for j in range(len(interPoints)):                             
                            if newLine[i] == interPoints[j]:                
                                xi = interPoints3D.xAt(j)
                                yi = interPoints3D.yAt(j)
                                zi = interPoints3D.zAt(j)
                                coordx = float(xi)                    
                                coordy = float(yi)   
                                coordz = float(zi)            
                                newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)                  
                                linelist.append(newcoord)                                
                                                        
                                coordpoint = [coordx, coordy, coordz]
                                linePoints3D.append(coordpoint)
                            
                                nextpos = pospointnewLine[i] + 1                                
                    else:                   
                        x0 = newLinexx[i][0]
                        y0 = newLinexx[i][1] 

                        typefeat = 1                            # 1 = line
                        #z0 = self.idw(x0, y0, linestring_line, typefeat) 
                        allpointsline = QgsLineString()    
                        allpointsline.setPoints(interlistx)   
                        allpointsline.append(linestring_line)  
                        z0 = self.idw(x0, y0, allpointsline, typefeat) 

                        coordx = float(x0)                    
                        coordy = float(y0)   
                        coordz = float(z0)      

                        #----------------------------------------------
                        if firstfeat != 1:
                            for j in range(len(linePoints3D1)):
                                xj = linePoints3D1[j][0]
                                yj = linePoints3D1[j][1]
                                if xj == x0 and yj == y0: 
                                    zj = linePoints3D1[j][2]                                    
                                    coordz = float(zj)  

                        #-----------------------------------------                   

                        newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                        linelist.append(newcoord)  

                        coordpoint = [coordx, coordy, coordz]
                        linePoints3D.append(coordpoint)

                #--------------------------------------------------                
                # reserves 3D points with the z coordinate calculated by iterpolating the line of the first feature:
                # to update the common points of the following features with the z coordinate of the first feature
                if firstfeat == 1:
                    linePoints3D1 = linePoints3D[:]   
               
                #-------------------------------------------------           
             
                newLine3D = QgsLineString()                     # QgsLineString
                newLine3D.setPoints(linelist)           
                linestring_line.append(newLine3D)              # joint points (line3D + newLine3D )     
      
                #------------  insert intersection points in line1        
                interLine1x = []  
                posinterLine1x = []
         
                for ind in range(len(segLine1)):
                    interdist = []  
                    interpoint = [] 
                    interpos = [] 
                    for ind1 in range(len(posinterLine1)): 
                        if posinterLine1[ind1] == ind:                     
                            interdist.append(distLine1[ind1])                    
                            interpoint.append(interPoints[ind1])
                            interpos.append(posinterLine1[ind1])
          
                    interdistx = interdist[:]
                    interdistx.sort()            
                    
                    for ind in range(len(interdistx)):
                        for ind1 in range(len(interdist)):
                            if interdistx[ind] == interdist[ind1]:
                                interLine1x.append(interpoint[ind1])
                                posinterLine1x.append(interpos[ind1]) 
              
                newLine1x = []        
                pospointnewLine1x = []                              
                
                for ind1 in range(len(segLine1)):        
                    newLine1x.append(segLine1[ind1][0])
                    pospointnewLine1x.append(0)   
                    ultpoint = segLine1[ind1][1]             
                            
                    for ind in range(len(posinterLine1x)):
                        if posinterLine1x[ind] == ind1:
                            newLine1x.append(interLine1x[ind])                     
                            ordem = self.sortpoints(ordemPoint, interLine1x[ind])                    
                            pospointnewLine1x.append(ordem)

                newLine1x.append(ultpoint)
                pospointnewLine1x.append(0)    
      
                #--------------   check position point in line1x
                ordem = 0 
                for i in range(len(pospointnewLine1x)):
                    if pospointnewLine1x[i] != 0:               
                        if ordem == 0:
                            ordemini = pospointnewLine1x[i]
                            ordem = ordemini
                        else:               
                            if ordemini != 1:
                                ordem -= 1 
                                if ordem != pospointnewLine1x[i]:
                                    QMessageBox.information(self.iface.mainWindow(), "View", "Reshape Line invalid" )   
                                    return 
                            else:
                                ordem += 1
                                if ordem != pospointnewLine1x[i]:
                                    QMessageBox.information(self.iface.mainWindow(), "View", "Reshape Line invalid" )   
                                    return 

                #-----------   create the final line  
                ninter= 0
                ninterl= 0
                ninterl1= 0  
                troca = 0              
                for ind in range(len(newLine)):
                    if pospointnewLine[ind] != 0:
                        ninterl += 1                
                        ninter=0
                        for ind1 in range(len(newLine1x)):
                            if pospointnewLine1x[ind1] != 0: 
                                ninter += 1
                                if newLine1x[ind1] == newLine[ind]:                           
                                    ninterl1 = ninter
                        if ninterl != ninterl1:
                            troca = 1                     # change line order
                            break

                if troca == 1:      
                    newLine.reverse() 
                    pospointnewLine.reverse()
        
                finalLine = []
                flagx = 0
                for i in range(len(newLine1x)):
                    if flagx == 0:
                        if pospointnewLine1x[i] != 0:
                            for j in range(len(newLine)):
                                finalLine.append(newLine[j])
                                ult = pospointnewLine[j]
                            flagx = 1 
                    
                        else:
                            finalLine.append(newLine1x[i]) 

                    else:
                        if ult == pospointnewLine1x[i]:   
                            flagx = 0
        
                lineList = []
                for points in finalLine:                
                    lineList.append(QgsPointXY(points))  

                finalLinex = QgsGeometry.fromPolylineXY(lineList)       # initial line with intersections points           
                finalLinexx = finalLinex.asPolyline()

                # ---------------    enter Z coordinate 
                totpoints = linestring_line.numPoints()
       
                linexlist = []
                for i in range(len(finalLine)):                
                    xi = finalLinexx[i][0]
                    yi = finalLinexx[i][1]

                    for j in range(totpoints):
                        xj = linestring_line.xAt(j)
                        yj = linestring_line.yAt(j)
                        if xj == xi and yj == yi: 
                            zj = linestring_line.zAt(j)
                            coordx = float(xi)                    
                            coordy = float(yi)   
                            coordz = float(zj)  
                            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                            linexlist.append(newcoord)
                            break        

                featx = QgsLineString() 
                featx.setPoints(linexlist)    
               
                #-------------------  features changed to update the layer  
                newfeat=QgsFeature()     

                newfeat.setGeometry(QgsGeometry(featx))  
                newfeat.setFields(feat.fields())
       
                for field in feat.fields():
                    newfeat.setAttribute(field.name(), feat[field.name()])  
        
                totfeat.append(newfeat)

        
            #**************************  FIM      reshape line     ********************************************

        else:            

            #******************************  INICIO   reshape polygon  **************************************************  
            
            pointsLinex = QgsGeometry.fromPolylineXY(pointsLine)        # LineString
            
            for feat in inViewFeats: 
                geom = feat.geometry()    
            
                if geom.intersects(pointsLinex):                   
                    geom1 = geom.asPolygon() 
                    # checks if the polygon contains rings and are intercepted by the line
                    if (len(geom1)) > 0:   
                        for pol in geom1[1:]:                    
                            ring = QgsGeometry.fromPolygonXY([pol])
                            if pointsLinex.intersects(ring):      
                                QMessageBox.information(self.iface.mainWindow(), "Error", "The line drawing intersects interior ring of polygon.")
                                return 

                    totgeom += 1  
                    ident = feat.id()
                    featidentx.append(ident)
                    featsActivex.append(feat) 
                
                    #------------------------------------                
                    #position of the line point on the feature (eg point 0 on the feature Id=23; point 4 on the feature Id=54)
                    pointOk = 0   
                    for i in range(len(pointsLine)):                    
                        if geom.contains(pointsLine[i]):                
                            firstPoint.append(i)                        
                            pointOk = 1                        
                            break
                    
                    if pointOk == 0:                    
                        firstPoint.append(fora)
                        fora += 1                    
                    #------------------------------------
                
            if totgeom == 0:
                QMessageBox.information(self.iface.mainWindow(), "Error", "The line drawing does not intersects the feature.")
                return  
         
            # first feature is the one with the lowest index or first point
            reserva = -1
            indice = 0
            for i in range(len(firstPoint)):            
                if reserva == -1:
                    reserva = firstPoint[i]  
                    indice = i
                else:     
                    if firstPoint[i] < reserva:
                        reserva = firstPoint[i] 
                        indice = i
        
            featident.append(featidentx[indice])
            featsActive.append(featsActivex[indice])  
                
            for i in range(len(featsActivex)): 
                if i != indice:
                    featident.append(featidentx[i])
                    featsActive.append(featsActivex[i])     

            totfeat = []          # features changed to update the layer
            firstfeat = 0         # indicates the feat with points 3D with the z coordinate to consider (first) to reserve  
        
            for i in range(len(featsActive)):  
                feat = featsActive[i]
                geom = feat.geometry()     
            
                firstfeat += 1     
                           
                #--------------    convert QgsPolygon3D to QgsLineString
                geom1 = geom.asPolygon()             
                geom3D = geom.asWkt()                            # QgsPolygon (PolygonZ)    coordenates (x,y,z)
                           
                if (len(geom1)) == 0:    
                    listgeom3D2 = self.retiracarater(geom3D)
                else:               
                    geom3D2 = self.retiracarater(geom3D)
                    geom3D2 = self.retiracarater(geom3D2)   
                    listgeom3D2 = geom3D2.split("),(")          #remove the characters "),("
            
                listvertex = []
                for i in range(len(listgeom3D2)):
                    vertex = listgeom3D2[i]
                    vertex = vertex.split(",")
                    listvertex.append(vertex)        
        
                geomlist=[] 
                for i in range(len(listvertex)):
                    geomlist.append(self.points3D(listvertex[i], regex))   

                linestring_polygon = QgsLineString()              # QgsLineString
                linestring_polygon.setPoints(geomlist[0])         # outer polygon vertices (x,y,z)

                #------------------------
        
                polygon0 = feat.geometry().asPolygon()[0]           # outer polygon vertices (x,y)
                pointsPolygon=[]                           
                for i in range(len(polygon0)):
                    pointsPolygon.append(polygon0[i])               # list of outer polygon vertices (x,y)

                segLine = self.segfeature(pointsLine)               #  splits line into segments         
            
                segPolygon = self.segfeature(pointsPolygon)         #  splits polygon into segments 
                       
                #-------------------- calculate intersection points    
                interlistx = []            
                interPoints = []         
                posinterPolygon = []   
                distPolygon = [] 
                posinterLine = []              
                distLine = []       
                for ind in range(len(segPolygon)):   
                    coordp0 = segPolygon[ind][0][0] 
                    coordp1 = segPolygon[ind][0][1]                
                    xxxPolygon = []   
                    xxxPolygon.append(segPolygon[ind][0])
                    xxxPolygon.append(segPolygon[ind][1])
           
                    segmentPolygon = QgsGeometry.fromPolylineXY(xxxPolygon)      
            
                    for ind1 in range(len(segLine)): 
                        coordl0 = segLine[ind1][0][0] 
                        coordl1 = segLine[ind1][0][1]
                        xxxLine = []
                        xxxLine.append(segLine[ind1][0])
                        xxxLine.append(segLine[ind1][1])
                        segmentLine = QgsGeometry.fromPolylineXY(xxxLine)               
                            
                        if segmentLine.intersects(segmentPolygon):  
                            inter = segmentLine.intersection(segmentPolygon)
                            vertice = inter.asPoint()   
                            interPoints.append(vertice)                   # intersection point

                            posinterLine.append(ind1)                     # line segment containing the point                   
                            dist = self.euclidean_distance(coordl0, coordl1, vertice[0], vertice[1] )                    
                            distLine.append(dist)                                        
                      
                            posinterPolygon.append(ind)                   #  polygon segment containing the point
                            dist = self.euclidean_distance(coordp0, coordp1, vertice[0], vertice[1] )                    
                            distPolygon.append(dist)
                    
                            #----------  insert Z coordinate at intersection points (interPoints3D)                        
                            interlist = []                 
                        
                            interx = vertice.x()                    # atribute .x() (access the x coordinate)
                            intery = vertice.y()                        
                        
                            segment3D = segmentPolygon.asPolyline()    

                            coordx = float(segment3D[0][0])             
                            coordy = float(segment3D[0][1])                                               
            
                            newcoord = self.interpointZ(coordx, coordy, geomlist[0], linestring_polygon) 
                            interlist.append(newcoord)  

                            coordx = float(segment3D[1][0])             
                            coordy = float(segment3D[1][1])    

                            newcoord = self.interpointZ(coordx, coordy, geomlist[0], linestring_polygon) 
                            interlist.append(newcoord)   
                    
                            interxx = QgsLineString()               # QgsLineString 
                            interxx.setPoints(interlist)
                    
                            typefeat = 1 
                            interz = self.idw(interx, intery, interxx, typefeat)  

                            coordx = float(interx)                    
                            coordy = float(intery)   
                            coordz = float(interz)                        
                            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                            interlistx.append(newcoord)                              

                if len(interPoints) < 2: 
                    QMessageBox.information(self.iface.mainWindow(), "Error", "To reshape a polygon it is necessary a line intersect 2 points in polygon.")
                    return   

                interPoints3D = QgsLineString()                     # QgsLineString 
                interPoints3D.setPoints(interlistx)     
            
                #---------------------  insert intersection points in line (newLine)         
                interLinex = []  
                posinterLinex = []
         
                for ind in range(len(segLine)):
                    interdist = []  
                    interpoint = [] 
                    interpos = [] 
                    for ind1 in range(len(posinterLine)): 
                        if posinterLine[ind1] == ind:                     
                            interdist.append(distLine[ind1])                    
                            interpoint.append(interPoints[ind1])
                            interpos.append(posinterLine[ind1])

                    interdistx = interdist[:]
                    interdistx.sort()                           # sort descending order
                            
                    for ind in range(len(interdistx)):
                        for ind1 in range(len(interdist)):
                            if interdistx[ind] == interdist[ind1]:
                                interLinex.append(interpoint[ind1])
                                posinterLinex.append(interpos[ind1])            
     
                newLine = []        
                pospointnewLine = []  
                ordemPoint = []       
                flaginter = 0                           
                ordem = 0        
                for ind1 in range(len(segLine)):      
                 
                    if flaginter == 1:
                        newLine.append(segLine[ind1][0])
                        pospointnewLine.append(0)                  
                      
                    for ind in range(len(posinterLinex)):
                        if posinterLinex[ind] == ind1:
                            ordem += 1
                            newLine.append(interLinex[ind])                    
                            pospointnewLine.append(ordem)   
                            ordemPoint.append(interLinex[ind]) 
                            flaginter = 1
             
                #-------------------   insert Z coordinate at line points (newLine3D)               
                newLinex = QgsGeometry.fromPolylineXY(newLine)
                newLinexx = newLinex.asPolyline()

                pospointnewLine9 = pospointnewLine[:]
                linelist = []
                linePoints3D = []
                coordpoint = []
                for i in range(len(newLine)): 
                    if pospointnewLine[i] != 0:                     
                        for j in range(len(interPoints)):                             
                            if newLine[i] == interPoints[j]:                
                                xi = interPoints3D.xAt(j)
                                yi = interPoints3D.yAt(j)
                                zi = interPoints3D.zAt(j)
                                coordx = float(xi)                    
                                coordy = float(yi)   
                                coordz = float(zi)                             
                                newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)                    
                                linelist.append(newcoord) 
                                                        
                                coordpoint = [coordx, coordy, coordz]
                                linePoints3D.append(coordpoint)
                            
                                nextpos = pospointnewLine[i] + 1  
                                       
                    else:
                        x0 = newLinexx[i][0]
                        y0 = newLinexx[i][1]               
              
                        typefeat = 2                            # 2 = polygon                    
                        allpointspolygon = QgsLineString()    
                        allpointspolygon.setPoints(interlistx)   
                        allpointspolygon.append(linestring_polygon)  
                        z0 = self.idw(x0, y0, allpointspolygon, typefeat)                
                   
                        coordx = float(x0)                    
                        coordy = float(y0)   
                        coordz = float(z0) 
                    
                        #----------------------------------------------
                        if firstfeat != 1:
                            for j in range(len(linePoints3D1)):
                                xj = linePoints3D1[j][0]
                                yj = linePoints3D1[j][1]
                                if xj == x0 and yj == y0: 
                                    zj = linePoints3D1[j][2]                                    
                                    coordz = float(zj)  
                        #-----------------------------------------                   

                        newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                        linelist.append(newcoord)     
                   
                        coordpoint = [coordx, coordy, coordz]
                        linePoints3D.append(coordpoint)
                    
                #--------------------------------------------------            
                # reserves 3D points with the z coordinate calculated by iterpolating the line of the first feature:
                # to update the common points of the following features with the z coordinate of the first feature
                if firstfeat == 1:
                    linePoints3D1 = linePoints3D[:]               
             
                newLine3D = QgsLineString()                     # QgsLineString
                newLine3D.setPoints(linelist)   
            
                linestring_polygon.append(newLine3D)              # join the points of the polygon and the line (polygon3D + newLine3D )

                #-----------------------  insert intersection points in polygon        
                interPolygonx = []  
                posinterPolygonx = []
         
                for ind in range(len(segPolygon)):
                    interdist = []  
                    interpoint = [] 
                    interpos = [] 
                    for ind1 in range(len(posinterPolygon)): 
                        if posinterPolygon[ind1] == ind:                     
                            interdist.append(distPolygon[ind1])                    
                            interpoint.append(interPoints[ind1])
                            interpos.append(posinterPolygon[ind1])
          
                    interdistx = interdist[:]
                    interdistx.sort()            
                      
                    for ind in range(len(interdistx)):
                        for ind1 in range(len(interdist)):
                            if interdistx[ind] == interdist[ind1]:
                                interPolygonx.append(interpoint[ind1])
                                posinterPolygonx.append(interpos[ind1]) 
           
                newPolygonx = []        
                pospointnewPolygonx = []                                
                
                for ind1 in range(len(segPolygon)):             
                    reservaPoint = segPolygon[0][0]  
                    newPolygonx.append(segPolygon[ind1][0])
                    pospointnewPolygonx.append(0)                        
                    for ind in range(len(posinterPolygonx)):
                        if posinterPolygonx[ind] == ind1:
                            newPolygonx.append(interPolygonx[ind])                     
                            ordem = self.sortpoints(ordemPoint, interPolygonx[ind])                    
                            pospointnewPolygonx.append(ordem)

                newPolygonx.append(reservaPoint)
                pospointnewPolygonx.append(0)   
        
                #------------  Change polygon start to 1st intersection                
                newPolygon = []
                pospointnewPolygon = []
                fimPolygon = []
                fimpospointPolygon = []
        
                ini = 0
                for ind in range(len(pospointnewPolygonx)-1):
                    if pospointnewPolygonx[ind] == 1:                               # first intersection
                        newPolygon.append(newPolygonx[ind])
                        pospointnewPolygon.append(pospointnewPolygonx[ind])
                        fimPolygon.append(newPolygonx[ind])  
                        fimpospointPolygon.append(pospointnewPolygonx[ind])
                        ini = 1
                
                    else:
                        if ini == 0:                                     
                            fimPolygon.append(newPolygonx[ind])                    # points before first intersection 
                            fimpospointPolygon.append(pospointnewPolygonx[ind])

                        else:
                            newPolygon.append(newPolygonx[ind])                    # points after first intersection
                            pospointnewPolygon.append(pospointnewPolygonx[ind])

               
                #if len(fimPolygon) > 0:
                for ind in range(len(fimPolygon)):
                    newPolygon.append(fimPolygon[ind])
                    pospointnewPolygon.append(fimpospointPolygon[ind])
           
                #-----------  Check if polygon order is equal to the line        
                ninter= 0
                ninterl= 0
                ninterp= 0  
                troca = 0              
                for ind in range(len(newLine)):
                    if pospointnewLine[ind] != 0:
                        ninterl += 1                
                        ninter=0
                        for ind1 in range(len(newPolygon)-1):
                            if pospointnewPolygon[ind1] != 0: 
                                ninter += 1
                                if newPolygon[ind1] == newLine[ind]:                           
                                    ninterp = ninter
                        if ninterl != ninterp:
                            troca = 1
                            break
      
                #------------  Create intersection polygons 
                newLine.reverse() 

                if troca == 1:      
                    newPolygon.reverse() 
                    pospointnewPolygon.reverse()
       
                subPolygon = []                      
                subPolygons = []               
                flagPolygon = 0
                pointIni=0
                pointFim=0
                for ind in range(len(newPolygon)): 
                    if pospointnewPolygon[ind] != 0:
                        if flagPolygon == 0:
                            pointIni = newPolygon[ind]
                            subPolygon.append(newPolygon[ind])
                            flagPolygon = 1                    
                        else:  
                            pointFim = newPolygon[ind]
                            subPolygon.append(newPolygon[ind])                                                    
                            flagLine = 0            
                            
                            for point in newLine:                                 
                                if point == pointFim:
                                    flagLine = 1                         
                                else:                                
                                    if point == pointIni:                   
                                        subPolygon.append(pointIni)
                                                                
                                        if flagLine == 1:                                    
                                            subPolygons.append(subPolygon)
                                        subPolygon = []
                                        flagLine = 0    
                                    else:    
                                        if flagLine == 1:                                   
                                            subPolygon.append(point)  
       
                            pointIni = pointFim
                            subPolygon.append(pointIni)
                            flagPolygon = 1

                    else:
                        if flagPolygon == 1: 
                            subPolygon.append(newPolygon[ind])

                #--------------   Create the new outer polygon        
                polyList = []
                for points in newPolygon:                 
                    polyList.append(QgsPointXY(points))   

                polyx = QgsGeometry.fromPolygonXY([polyList])     # initial polygon with intersections points       
     
                possubPolygons = [] 
                subpolyList = []        
                for ind in range(len(subPolygons)):            
                    subpolyListx = []
                    for points in subPolygons[ind]:
                        subpolyListx.append(QgsPointXY(points))
            
                    subPoly = QgsGeometry.fromPolygonXY([subpolyListx]) 
             
                    if polyx.contains(subPoly):                
                        possubPolygons.append(1)     # inside
                    else:  
                        possubPolygons.append(0)     # outside
            
                    if not subPoly.isGeosValid(): 
                        QMessageBox.information(self.iface.mainWindow(), "View", "Geometry is not valid.")  # + str(subpolyListx))    
                        return     
                   
                    subpolyList.append(subpolyListx)       
            
                listxx = []
                for ind in range(len(subpolyList)):
                    listxx=subpolyList[ind]
                    subPoly = QgsGeometry.fromPolygonXY([listxx])          

                    if possubPolygons[ind] == 1:                          
                        lengthsubPoly=subPoly.length()                               
                        polyx=polyx.difference(subPoly)          
                        lengthpolyx=polyx.length()  
                        if lengthpolyx < lengthsubPoly:
                            polyx=subPoly 
                    else:                      
                        polyx=polyx.combine(subPoly)  

                # -------------------  convert QgsPolygon to QgsLineString            
                poly3D = polyx.asWkt()
        
                listpoly3D2 = self.retiracarater1(poly3D)
        
                listvertex1 = []
                for i in range(len(listpoly3D2)):
                    vertex = listpoly3D2[i]
                    vertex = vertex.split(",")
                    listvertex1.append(vertex)

                geomlist1=[] 
                for i in range(len(listvertex1)):
                    geomlist1.append(self.points3D(listvertex1[i], regex))     
  
                linestring_polyx = QgsLineString()            # QgsLineString
                linestring_polyx.setPoints(geomlist1[0])   

                totpointsx = linestring_polyx.numPoints()

                # -----------------  enter Z coordinate
                totpoints = linestring_polygon.numPoints()
       
                polyxlist = []
                for i in range(totpointsx):
                    xi = linestring_polyx.xAt(i)
                    yi = linestring_polyx.yAt(i)

                    for j in range(totpoints):
                        xj = linestring_polygon.xAt(j)
                        yj = linestring_polygon.yAt(j)
                        if xj == xi and yj == yi: 
                            zj = linestring_polygon.zAt(j)
                            coordx = float(xi)                    
                            coordy = float(yi)   
                            coordz = float(zj)         
                            newcoord = QgsPoint(coordx, coordy, coordz, wkbType=QgsWkbTypes.PointZ)  
                            polyxlist.append(newcoord)
                            break

                linestring_polyx = QgsLineString()     # QgsLineString           
                linestring_polyx.setPoints(polyxlist)  

                featx = QgsPolygon()             
                featx.setExteriorRing(linestring_polyx) 

           
                # ------------------ inner polygons (add rings)            
                if len(geomlist) > 0:   
                    for i in range(len(geomlist)):
                        if i != 0:                         
                            linestring_ring = QgsLineString()         # QgsLineString
                            linestring_ring.setPoints(geomlist[i])                     
                            featx.addInteriorRing(linestring_ring)       
            
                #-------------------  features changed to update the layer  
                newfeat=QgsFeature()     

                newfeat.setGeometry(QgsGeometry(featx))  
                newfeat.setFields(feat.fields())
       
                for field in feat.fields():
                    newfeat.setAttribute(field.name(), feat[field.name()])  
        
                totfeat.append(newfeat)

           
                #***************************  FIM    reshape polygon   ********************************************-   
        
        layer.startEditing()

        for i in range(len(totfeat)):
            layer.addFeature(totfeat[i])
            newfeat = totfeat[i]  
            ident = featident[i]         
            layer.deleteFeature(ident)         
            layer.selectByIds( [newfeat.id()] )  
                        
        layer.triggerRepaint()          
    
        self.iface.messageBar().pushMessage("CIGeoE Reshape Features 3D", "Reshape Features 3D done" , level=Qgis.Info, duration=2)
        
        


